# SOLID PRINCIPLES #

---
# Introduction #
The SOLID principles are five key principles of Object-Oriented Programmed (OOP). It is an acronym that stands for:
-	S – Single Responsibility Principle
-	O – Open-Closed Principle
-	L – Liskov Substitution Principle
-	I – Interface Segregation Principle
-	D – Dependency Inversion Principle

---
# Principles #

---
## Single Responsibility Principle ##

**“A class should have one and only one reason to change, meaning that a class should have only one job.”
The simplest principle; Each class should do one job.**

As an example, an a First-Person Shooter game, the player controller may contain how much health a player has, but should NOT also write that information to the GUI. Instead, the player controller should just pass the information to a separate class, which should in turn write to the GUI.
 

---
## Open-Closed Principle ##

**“Objects or entities should be open for extension but closed for modification.”**

Essentially, you should be able to extend the class without changing the class itself. This means that you should avoid hard-coding values or having methods be too specific the current scenario. 

---
## Liskov Substitution Principle ##

**“Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T.”**

This principle is very verbosely written but what it means is that you should be able to substitute any subclass with its parent class.

---
## Interface Segregation Principle ##

**“A client should never be forced to implement an interface that it doesn’t use, or clients shouldn’t be forced to depend on methods they do not use.”**

This principle is actually self-explanatory; Don’t force the users of an interface to implement methods that they won’t use.
For example, if you have a class representing a computer, do not add an “ejectDisk()” method, because not all computers have disk drives. You should only include methods that are universal. The computers with disk drives can implement a separate “DiskUser” interface for that functionality.

---
## Dependency Inversion Principle ##

**“Entities must depend on abstractions, not on concretions. It states that the high-level module must not depend on the low-level module, but they should depend on abstractions.”**

This principle relates to decoupling. In essence, code using interfaces where possible. Use high-level classes and concepts until you need the specificity.

---
# Resources #
- [SOLID: The First 5 Principles of Object Oriented Design](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design#:~:text=SOLID%3A%20The%20First%205%20Principles%20of%20Object%20Oriented,5%20Dependency%20Inversion%20Principle.%20...%206%20Conclusion.%20)
- [A Solid Guide to SOLID Principles](https://www.baeldung.com/solid-principles)
- [SOLID Principles — explained with examples](https://medium.com/mindorks/solid-principles-explained-with-examples-79d1ce114ace)