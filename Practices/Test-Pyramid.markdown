# TEST PYRAMID #
![alt text](Practices/img/TestPyramid.png "Test Pyramid Diagram")

---
# Introduction #
The Test Pyramid is a simple concept which describes good practice for testing.
Simply put, we want to do a lot of quick, cheaper tests and fewer tests that take longer and are more expensive to perform.

---
# Principles #
The idea of the test pyramid is that when a test is time consuming, and therefore expensive to perform, we naturally want to rely on those tests as little as possible.
Whereas the tests that are run automatically, don't take long to run and don't take long to write, well those are great and we want lots of those.

---
## UI, End to End, or Acceptance Tests ##
These terms are often used to covered roughly the same level of testing.
We want to test the full journey through our software which can look different from project to project but as an example:

A single test might cover:
- Hitting an endpoint
- Creating a message on a kafka topic
- Performing some sort of data manipulation with the kafka message in another system
- Adding metrics messages to a different kafka topic
- Giving back a response body that indicates the success of the operation

A test like that can take a while to write. It can take a while to run, since it will probably involve spinning up a test environment.
There are more processes to perform and there will likely need to be some delays in the tests to make sure the message is on the kafka topic, etc.

This test is therefore fairly expensive in both money and computing terms. It can also be fairly brittle and break when new code changes are made.

These tests are crucial, because they tell us that the project is actually doing what it should be doing and that is critical information, but we shouldn't use big tests like this to cover every aspect of our code.

---
## Integration Tests ##
Integration testing is checking that any two components have properly integrated together.

There are many possible examples for in the simplest form, it could be that one of your Java methods, which reaches out to call a Java method in another class, is working properly.

These tests are much quicker to write and easier to maintain than a full suite of End to End tests but don't cover the same breadth.
They also provide less granular detail than a unit test.

---
## Unit Tests ##
Unit Tests are design to test the single smallest component, or unit. This is typically a single method.

The main advantages of unit tests are:
- They provide very precise detail. If this one method if given an input of X, do I get the output of Y that I expect?
- They are easy to maintain. Most unit tests don't need to be changed when functionality changes. Only if the one tested method changes.
- They are typically quick to write. If the purpose of a method is clear, then writing a test for it shouldn't take too long.
- They are the easiest to automate and quickest to run. A collection of 100+ unit tests will probably only take a few seconds to run.

For these reasons, we want lots of them. Most methods should have Unit Tests.
The more Unit Tests we have, the fewer Integration Tests we need to rely on.
The more Integration Tests we have, the fewer End to End Tests we need to rely on.

---
# Resources #
- [Better Programming - The Test Pyramid](https://betterprogramming.pub/the-test-pyramid-80d77535573)
- [Youtube - What is the Testing Pyramid Even?](https://www.youtube.com/watch?v=ndMYYxP_Gzs)
- [SmartBear - What is End to End Testing?](https://smartbear.com/solutions/end-to-end-testing/)
- [Wikipedia - Integration Testing](https://en.wikipedia.org/wiki/Integration_testing)
- [SmartBear - What is Unit Testing?](https://smartbear.com/learn/automated-testing/what-is-unit-testing/)