# ACID PRINCIPLES #

![alt text](Practices/img/ACID.png "Acid Clipart")

---
# Introduction #
The ACID properties are four key principles of and SQL database (or other relational database). It is an acronym that stands for:
-	A – Atomic
-	C – Consistent 
-	I – Isolation
-	D – Durable

---
# Properties #

---
## Atomic ##

**“Transaction acting on several pieces of information complete only if all pieces successfully save. Here, “all of nothing” applies to the transaction”**

Simply, transactions must complete entirely or not at all. The simplest example is that when moving money from one account to another, the deduction should not take place until the money has successfully reached the second account.

---
## Consistent ##

**“The saved data cannot violate the integrity of the database. Interrupted modifications are rolled back to ensure the database is in a state before the change takes place.”**

Essentially, the database should only ever live in a state that is consistent with the rules. Incorrect data cannot be added, or must be immediately removed if it is added. 

---
## Isolation ##

**“No other transactions take place and affect the transaction in question. This prevents “mid-air collision.”**

Simply put, transaction must take place sequentially. Operations cannot happen concurrently. Otherwise, there is no way to ensure the accuracy of the transaction.

---
## Durable ##

**“System failures or restarts do not affect committed transactions.”**

Transactions cannot simply be lost if there is a power outage or system failure part way through.

---
# Resources #
-	[SQL ACID Database Properties Explained](https://www.essentialsql.com/sql-acid-database-properties-explained/#:~:text=The%20ACID%20database%20properties%20define%20SQL%20database%20key,Isolation%2C%20and%20Durability.%20Here%20are%20some%20informal%20definitions%3A)
-	[The ACID Model for Database Management Systems](https://www.lifewire.com/the-acid-model-1019731)
-	[What does ACID mean in Database Systems?](https://database.guide/what-is-acid-in-databases/)
