# HEXAGONAL ARCHITECTURE #

---
# Introduction #
Hexagonal architecture is a coding design principle used to create more structured code that is easier to maintain and develop.

![alt text](Practices/img/HexArch.drawio.png "Hexagonal Architecture Diagram")

---
# Principles #
In essence, the idea of Hexagonal architecture is very simple.

Hexagonal architecture is about splitting a project into several components that are loosely-coupled and all feed back to the core application. As an example, these might be the core business logic, a service, and a database. By being loosely-coupled, these components can be easily swapped out for alternate implementations without affecting the other components.

- Each component is typically referred to as an "adapter" ion the context of hexagonal architecture 
- There really isn't much to Hexagonal architecture other than loose-coupling. If you understand loose-coupling then you understand hexagonal architecture.
- **NB:** There is nothing hexagonal about it. The standard diagrams just use a hexagon, even though they also only use four sides to represent different areas of the program.

---
# Resources #
-	[Wikipedia - Hexagonal Architecture (software)](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software))
-	[Netflix Blog - Ready for Changes with Hexagonal Architecture ](https://netflixtechblog.com/ready-for-changes-with-hexagonal-architecture-b315ec967749)
