# Problem #
Sometimes, a namespace will get stuck in the "Terminating" state which can stop you from doing anything.


# Fix #

To delete a namespace stuck in terminating:


- Get the namespace description and put it into a temporary file
```
kubectl get <NAMESPACE TO CHANGE> keda -o json > tmp.json
```

- Alter the file
Remove Kubernetes line from finalisers section

```
vim tmp.json
```

- Replace the existing namespace description with your new file
```
kubectl replace --raw "/api/v1/namespaces/keda/finalize" -f ./tmp.json
```

Repeat for any other stuck namespaces