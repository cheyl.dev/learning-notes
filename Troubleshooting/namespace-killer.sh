#!/bin/bash

# Vars
filename="testFile.json"
tempFile="tmp.json"
search='"kubernetes"'
replace=""
read -p "What namespace do you want to kill?" namespace

 
# Put the Namespace description into a file
kubectl get namespace $namespace -o json > $tempFile

 
# Delete the Finalizer
sed "s/$search/$replace/" $filename > $filename


# Replace the original with that file
kubectl replace --raw "/api/v1/namespaces/$namespace/finalize" -f ./$tempFile #This path might need changing

 
# Delete the temp file
rm $tempFile
