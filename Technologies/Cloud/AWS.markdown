# Introduction #
![alt text](Technologies/Cloud/img/AWS.png "AWS Logo")

# Beginner's Guide #
- This guide gives an overview of what AWS is, how to interact with it through the Management Console and CLI, and an overview of the most common aWS services.
- The guide includes links to relevant reading materials and AWS documentation for more detail.
---
## General Concepts ##

### What is AWS? ###
[AWS - What is AWS?](https://aws.amazon.com/what-is-aws/)

- Amazon Web Services is a cloud platform that provides a collection of services that allow you to create your infrastructure without managing your own physical machines.

- These services can be accessed using the [AWS Management Console](https://aws.amazon.com/console/). 
  - You will need an account for this. This process requires you to input card details and provide account verification.

- AWS works on a "pay as you go" model, so if your machine is not currently running, then it isn't costing you anything. 
  - As you would expect, more computing power will cost more than a weaker machine.

---
### The AWS Console ###
[AWS Management Console](https://aws.amazon.com/console/)
- The AWS Mangement Console is a browser-based GUI for accessing AWS services.
- From here, you can create new EC2 instances, manages users, and anything else that you might want to do.

---
### The AWS CLI (Command Line Interface) ###
[AWS Command Line Interface](https://aws.amazon.com/cli/)
[AWS Docs - What is the Command Line Interface?](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

- The CLI is an open-source tool that lets you interact with your AWS services using the command line terminal.
- You can create the same setups that you can through the Management Console but this provides an alternative.
- This can potentially be quicker than the Management Console for certain uses, if you know the right command.

#### Installing The ClI (Command Line Interface) ####
- Installation steps can be found [here](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- For Linux users, just perform the following commands
````
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
````

---
### Regions Availability Zones ###
[AWS Docs - Regions and Zones](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions)

#### Regions ####
- A region is essentially a collection of data centers that cover a wide area, such as the "eu-west-1" which is based in London.
- Services that you want to be able to talk to each other must be created in the same region.
- Your current region can be seen, and changed, in the top right corner of the AWS Management Console

#### Availability Zones ####
- An Availability Zone is one area within a Region.
- When creating instances, you can either specify an AZ or just have one chosen if it doesn't matter.
- You can design your system to have instances running in each availability zone as a form of redundancy, with traffic being routed from a downed instance into an active instance instead.

---
## Services ##
This section will give an overview of some of the most important AWS services in a loose order of importance.


````
TODO: Split by types. IE Compute, Networking, Databases
````
---

[Eckher - List of AWS Services](https://www.eckher.com/c/21gjdl7gz4)
This link provides a list of all the services within AWS and links to their documentation

---
### EC2 (Elastic Cloud Compute) ###
[AWS Docs - What is Amazon EC2?](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)

- EC2 is the simplest AWS service. An EC2 instance is simply a Virtual Machine, or VM.
- You can create a machine, selecting from a list of operating systems.
- These operating system images are called Amazon Machine Images (AMIs) and we'll expand slightly on that below.

---
#### AMIs (Amazon Machine Images) ####
[AWS Docs - Amazon Machine Images](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html)

- Amazon Machine Images are the operating system images used to create an EC2 instance.
- By default, there are many to choose from that will cover most of your needs, but once you've spun up a machine and installed various things on it, you may wish to create your own AMI so that you can reproduce it in the future.
- We won't go into detail here on how to do that, but it is relatively straight-forward to do from within the AWS Console, and is fairly intuitive.

---
### EKS (Elastic Kubernetes Service) ###

[AWS Docs - What is EKS?](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html)

[AWS Docs - Getting Started with Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)

- Amazon Elastic Kubernetes Service is an instance of Kubernetes that you don't need to install and maintain yourself.
- Integrated with other AWS services, you can use Elastic Load Balancing to distribute load, and IAM (See below) for authentication.

---


### Dynamo DB ###
````
TODO
````
---

### Aurora ###
````
TODO
````
---


### S3 (Simple Storage Solution) ###
[AWS Docs - What is Amazon S3?](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html)

- Often referred to as an "S3 Bucket"
- S3 is an object store, where "object" just means a file. In essence, it's a hard drive you can access remotely with get requests
- A common use of S3 is hosting static websites. By uploading your HTML,CSS etc, you can serve your website directly from the S3 bucket.
- Can automatically move old data into long-term storage as a cheaper solution.

---
### RDS (Relational Database Service) ###
[AWS Docs - What is RDS?](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html)

- RDS gives you a database, without the need to manage the server that it's running on like you would with an on-premises database.
- Like all AWS services, it is easily scalable if you need more storage, or higher input/output volume
- RDS supports the following database engines:
  - MySQL
  - MariaDB
  - PostgreSQL
  - Oracle
  - Microsoft SQL Server

--- 
### IAM (Identity and Access Management) ###
[AWS Docs - What is IAM?](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)

- IAM allows you to control access to your other AWS resources.
- You can control who is signed in, to manage the identity of people interacting with your services.
- You can then control the permissions that each person has.
- Allows you to share your account without giving away your credentials.
- You can also add two-factor authentication to your account or a user for extra security.
- IAM is always free. It is the other services being accessed where charges are incurred.

---
### Cognito ###
[AWS Docs - What is Cognito?](https://docs.aws.amazon.com/cognito/latest/developerguide/what-is-amazon-cognito.html)

- Cognito provides a mechanism for users to be authenticated using Amazon, Apple, Facebook or Google credentials.
- The users will then receive AWS credentials from a Cognito User Pool, with which they can then access other AWS services

---
### Cloudfront ###
[AWS Docs - What is Cloudfront?](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)

- Cloudfront is a mechanism for speeding up the delivery of your web content
- Cloudfront distributes your content to various datacenters so that when a request is made, the content is pulled from the nearest location to speed up delivery.
- If the content is not already in that datacentre, then it will be pulled from a location that you have determined (such as an S3 bucket) to serve the most up to date content.

---
### VPC (Virtual Private Cloud) ###
[AWS Docs - What is Amazon VPC?](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
````
TODO
````

---
### SNS (Simple Notification Service) ###
[AWS Docs - What is SNS?](https://docs.aws.amazon.com/sns/latest/dg/welcome.html)
````
TODO
````

---
### Elastic Beanstalk ###
[AWS Docs - What is Elastic Beanstalk?](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html)

````
TODO
````

### Route53 ###
[AWS Docs - What is Route 53?](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)

This video gives a really clear description of what DNS is, what Route 53 is and what it can do.
[Youtube - What is DNS? - Introduction to Domain Name System](https://www.youtube.com/watch?v=e2xLV7pCOLI)

- Route 53 is a DNS server that lets you route your domain name, an AWS application.
- You can buy and maintain your domain name through AWS, which is quicker and easier than buying it though an external registrar.
- Integrates easily with other AWS services.
- As with all AWS services, the availability is 100% due to the redundancy that they have in place.
---

### Lambda ###
[AWS Docs - What is AWS Lambda?](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)

- Lambdas are single functions that can be run without managing an entire server.
- A Lambda is spun up in response to a request, and can only live up to fifteen minutes, so are best-suits to short, event driven use-cases.
- Very easy to set up. The AWS Management Console allows you to edit your code directly, for certain support languages like Python.
  - With some other languages that are supported by the Lambda runtime, but not the code editor, you have to upload code. For example, Go.
---

# Resources #
- [Youtube - What is AWS? | Amazon Web Services](https://www.youtube.com/watch?v=a9__D53WsUs)
- [Amazon - What is AWS?](https://aws.amazon.com/what-is-aws/)
- [AWS Management Console](https://aws.amazon.com/console/)
- [AWS Command Line Interface](https://aws.amazon.com/cli/)
  [AWS Docs - What is the Command Line Interface?](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
- [AWS Docs - Installing or updating the latest version of the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [AWS Docs - Regions and Zones](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions)
- [Eckher - List of AWS Services](https://www.eckher.com/c/21gjdl7gz4)
- [AWS Docs - What is Amazon EC2?](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
- [AWS Docs - Amazon Machine Images](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html)
- [AWS Docs - What is EKS?](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html)
- [AWS Docs - Getting Started with Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)
- [AWS Docs - What is Amazon S3?](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html)
- [AWS Docs - What is RDS?](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html)
- [AWS Docs - What is IAM?](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
- [Youtube - Practical Projects to Learn AWS](https://www.youtube.com/watch?v=06VgLTqNvU8)
- [AWS Docs - What is Cognito?](https://docs.aws.amazon.com/cognito/latest/developerguide/what-is-amazon-cognito.html)
- [AWS Docs - What is Cloudfront?](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)
- [AWS Docs - What is Amazon VPC?](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
- [AWS Docs - What is SNS?](https://docs.aws.amazon.com/sns/latest/dg/welcome.html)
- [AWS Docs - What is Elastic Beanstalk?](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html)
- [AWS Docs - What is Route 53?](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)
- [Youtube - What is DNS? - Introduction to Domain Name System](https://www.youtube.com/watch?v=e2xLV7pCOLI)
- [AWS Docs - What is AWS Lambda?](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
- [Youtube - React App on AWS S3 with Static Hosting + Cloudfront | Practical AWS Projects #1](https://www.youtube.com/watch?v=mls8tiiI3uc)
- [Youtube - Build a Docker based Flask App with Aurora Serverless | Practical AWS Projects #2](https://www.youtube.com/watch?v=NM4Vd7fpZWk)