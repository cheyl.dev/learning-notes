# Java #


# Introduction #
This guide is a quick overview of Java.

The guide won't go into a huge amount of detail but should provide enough examples that if you know another programming language already, then picking up Java should be relatively simple.
I basically want to provide examples of all the basic things that you would want to google the formatting of.

---
# Beginner's Guide #

## Declaring & Assigning Variables ##
- Variables can loosely be two different types:
    - Primitive Datatypes
    - Objects

- In either case, the format is: ```type name;```
    - Primitive Datatypes will be all lower case
    - Objects will start with a capital
        - Strings are an object

- Assigning a value to a variable is easy. Just do ```variable = value;```.
- Assignment can be done at the same time as declaration.


Examples
````
int myInt;
myInt = 3;
OR 
int myInt = 3;

String hellowWorld = "Hello World!"
Object obj = new Object();

````

---
## Basic Arithmetic ##
Basic mathematic operators in Go are exactly what you'd expect. +,-,*,/
```
int addition = num1 + num2;
int subtraction = num1 - num2;
int multiplication = num1 * num2;
int division = num1 / num2;
```
---
## Lists & Arrays ##

TODO

### Arrays ###
#### Declaring Arrays ####
##### Basic Declaration #####
#### Declaring Arrays With Values ####
#### Get the Size of a Array ####

### Lists ###
#### Declaring Lists ####
##### Basic Declaration #####
#### Declaring Lists With Values ####
#### Get the Size of a List ####


---
## If Statements ##
- If statements are exactly what you'd expect.

- TODO: Explain if statements



---
## For Loops ##
For loops work in Go in similar ways to other languages.
Define a new int. Define the range you want to iterate over. Increment/Decrement the int.

```
if (int i = 0 ; i < 5; i++) {
    // Do Stuff
}
```

- The declaration is split into three chunk, separated by semicolons
    - **First, declare an integer.** "i" is a new integer that the loop uses to count how many iterations it has done.
    - Second, under what condition should the code be run again? Here, if "i" is less than five, the code will run.
    - Thirdly, what happens after each iteration of the loop? In this example, "i" will increase by one.
- The overall effect of this is that "// Do stuff" will happen five times.
---

## Methods ##
- TODO: Expand on this

### Return types ###
### Overloading ###

## Classes ##
- TODO: Expand on this

### Inheritance ###
- TODO: Expand on this

### Interfaces ###
- TODO: Expand on this



---
# Resources #
