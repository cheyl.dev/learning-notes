package main

import "fmt"


func main() {
    declareEmptySlice()
    declareFixedSlice()
    declareSliceWithValues()
    shortDeclareSliceWithValues()
    gettingRangeOfValuesFromSlice()
    appendValueToSlice()
    getSizeOfSlice()
}

func declareEmptySlice() {
    var mySlice []string
	fmt.Println(mySlice)
}

func declareFixedSlice() {
    var mySlice [2]string
    mySlice[0] = "Hello"
    mySlice[1] = "World"
    // setSizeSlice[5] = 10
    // This would throw an invalid array index (out of bounds) exception
	fmt.Println(mySlice)
}

func declareSliceWithValues() {
    var mySlice []string = []string{"Hello","World",}
	fmt.Println(mySlice)
}

func shortDeclareSliceWithValues() {
    mySlice := []string{"Hello","World"}
   	fmt.Println(mySlice)
}

func gettingRangeOfValuesFromSlice() {
    var mySlice []int = []int{1,2,3,4,5}
    var mySecondSlice []int = mySlice[0:3]
   	fmt.Println(mySecondSlice)
}

func appendValueToSlice(){
    var mySlice []string = []string{"Hello"}
    mySlice = append(mySlice, "World")
    // You cannot append if the slice has a defined size
    fmt.Println(mySlice)
}

func getSizeOfSlice(){
    var mySlice []int = []int{1,2,3,4,5,6,7,8}
    fmt.Println(mySlice)
    fmt.Println(len(mySlice))
}