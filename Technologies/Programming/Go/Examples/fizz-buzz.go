package main

import "fmt"

type matchCondition struct
{
    num int;
    word string;
}

var conditionList []matchCondition

func main() {
    var fizz matchCondition = matchCondition{3,"fizz"}
    var buzz matchCondition = matchCondition{5,"buzz"}

    conditionList = append(conditionList, fizz)
    conditionList = append(conditionList, buzz)

    processListOfNumbers(100, conditionList)
}

func processListOfNumbers (valueRange int, conditions []matchCondition) {
    for i := 1; i <= valueRange; i++ {
        for j := 0; j < len(conditions); j++ {
            processNumber(i, conditions[j])
        }
    }
}

func processNumber (input int , condition matchCondition) {


    if (input % condition.num == 0) {
        fmt.Println(fmt.Sprint(input, " ", condition.word))
    }
}