package main

import "fmt"

var HELLO string = "Hello"
var WORLD string = "World"

func main() {
    // This is the short variable declaration. The := make it take he implicit type of the right-hand side
    // This can only be done inside methods
    exclamation := "!"
	fmt.Println(HELLO + " " + WORLD + exclamation)
}