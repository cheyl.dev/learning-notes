package main

import (
    "fmt"
    "log"
    "net/http"
)

func homePage(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Welcome to the HomePage!")
    fmt.Println("Endpoint Hit: homePage")
}

func otherPage(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Welcome to the other page!!!!")
    fmt.Println("Endpoint Hit: Other")
}

func handlePages() {
    http.HandleFunc("/", homePage)
    http.HandleFunc("/other", otherPage)
    log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
    handlePages()
}