package main

import "fmt"

var HELLO string = "Hello"
var WORLD string = "World"

func main() {
	fmt.Println(HELLO + " " + WORLD + "!")
}