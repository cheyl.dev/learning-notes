package main

import "fmt"

func main() {

// Here, I am using "const" instead of "var" which makes the value final
    const NUM_ONE int = 10
    const NUM_TWO int = 5

	fmt.Println(fmt.Sprint("Addition: ", add(NUM_ONE, NUM_TWO)))
	fmt.Println(fmt.Sprint("Subtraction: ", subtract(NUM_ONE, NUM_TWO)))
	fmt.Println(fmt.Sprint("Multiplication: ", multiply(NUM_ONE, NUM_TWO)))
	fmt.Println(fmt.Sprint("Division: ", divide(NUM_ONE, NUM_TWO)))
}

func add (num1 int, num2 int) int {
    return num1 + num2
}

func subtract (num1 int, num2 int) int {
    return num1 - num2
}

func multiply (num1 int, num2 int) int {
    return num1 * num2
}

func divide (num1 int, num2 int) int {
    return num1 / num2
}