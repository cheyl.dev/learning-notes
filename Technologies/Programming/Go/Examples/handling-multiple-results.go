package main

import "fmt"

func main() {

    var NUM_ONE int = 10
    var NUM_TWO int = 5

    var sum, diff = processNumber(NUM_ONE, NUM_TWO)

    fmt.Println(fmt.Sprint("Sum: ", sum))
    fmt.Println(fmt.Sprint("Dif: ", diff))
}

// This demonstrates two things in one do:
// - Variables can be declared in the return type field, so a naked return will return those values
// - A method in go can return multiple values together
func processNumber (num1 int, num2 int) (sum, diff int) {
    sum = num1 + num2
    diff = num1 - num2
    return sum, diff
}