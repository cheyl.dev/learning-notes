# GOLANG #
![alt text](Technologies/Programming/img/golang-logo.png "Go Logo")

---
# Introduction #
This guide is a quick overview of installing Go, writing a script and then running it.

The guide won't go into a huge amount of detail but should provide enough examples that if you know another programming language already, then picking up Go should be relatively simple.
I basically want to provide examples of all the basic things that you would want to google the formatting of.

---
# Beginner's Guide #

---
## Installing Go (Linux) ##
For the most part, just follow **[this](https://golangdocs.com/install-go-linux)** quick guide.

Additionally, if go commands do not work when you load new terminals, then try adding the ``export PATH=$PATH:/usr/local/go/bin`` command to your .bashrc file as well. This worked for me.

---
## Creating your file ##
- The file extension for go files is .go
- Each class must have a package
- For single-class projects, you must use the "main" package and include a method called "main"

#### Example ####

````
package main

import "fmt"

func main() {
fmt.Println("Hello World!")
}
````
---
## Declaring Variables ##
- Slightly different to Java, the format is: ``var <name> <type> = <value>``
- Commands in Go do not require a semicolon (;)

````
var myString string = "My String"
var myInt int = 99
var myBool bool = true
var myFLoat float32 = 2.4
````
---
## Basic Arithmetic ##
Basic mathematic operators in Go are exactly what you'd expect. +,-,*,/
```
var addition int = num1 + num2;
var subtraction int = num1 - num2;
var multiplication int = num1 * num2;
var division int = num1 / num2;
```
---
## Slices ##
"Slices" are essentially somewhere between an Array and a List in Go.
You can define the size of a slice up front, like an Array, but you can also declare them as empty and add new values dynamically, like you would with a List.

- In this repo, **Technologies/Programming/Go/Examples/slices.go** contains some examples of each of the following points

#### Declaring Slices ####
##### Basic Declaration #####
- Just add square brackets to the beginning of the variable type.
- You can define the size of the slice at the same time
- To access a value in a slice, put a number in the square brackets
```
var mySlice [2]string
mySlice[0] = "Hello"
mySlice[1] = "World"
```

#### Declaring Slices With Values ####
- You can define values when you declare the slice, by specifying the type and putting the values in braces.
- Below are two ways of writing the same slice declaration

```
mySlice []string = []string{"Hello","World",}
mySlice := []string{"Hello","World"}
```

#### Add Values to a Slice ####
- To add a value to a slice, use the "append" method
- You cannot do this if the slice already has a defined size

```
var mySlice []string = []string{"Hello"}
mySlice = append(mySlice, "World")
```
#### Get the Size of a Slice ####
- To find the size of a slice, simply use "len" (short for "length")

```
var mySlice []int = []int{1,2,3,4,5,6,7,8}
var size int = len(mySlice)
```
---
## If Statements ##
If statements are exactly what you'd expect.

```
if (input % condition.num == 0) {
    // Do Stuff
}
```
---
## For Loops ##
For loops work in Go in similar ways to other languages.
Define a new int. Define the range you want to iterate over. Increment/Decrement the int.
```
for i := 0; i <= limit; i++ {
    // Do Stuff
}
```
---
## Methods ##
- TODO: Expand on this

---
# Running Go Scripts #
Once you've written your script, you'll want to run it.

To run your script, just execute: ``go run <script>``
```
go run hello-world.go
```

---
# Building Executables #
Once you've finished writing your code completely, you'll likely want to build it, ready for deployment.
To build your code, execute: ``go build <script>``
```
go run hello-world.go
```
This will produce a binary in the same directory as the original code.

---
# Resources #
- [Installing Go](https://golangdocs.com/install-go-linux)
- [Wikipedia - Go](https://en.wikipedia.org/wiki/Go_(programming_language))
- [Go Resources - An introduction to programming in Go](https://www.golang-book.com/books/intro)
- [A Tour of Go](https://tour.golang.org/basics/1)
- In this repo, see Technologies/Programming/Go/Examples for some small example scripts